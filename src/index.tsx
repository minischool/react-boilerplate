import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Login from 'pages/Login';
import Home from 'pages/Home';
import 'bootstrap/dist/css/bootstrap.min.css';

if (process.env.NODE_ENV !== 'development') {
  console.debug = () => {};
  console.log = () => {};
  console.warn = () => {};
}

class App extends React.Component {
  render() {
    return (
      <Router>
        <Route exact path="/" component={Home} />
        <Route path="/login" component={Login} />
      </Router>
    );
  }
}

render(<App />, document.getElementById('root'));
