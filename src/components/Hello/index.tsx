import React from 'react';

type HelloProps = {
  /** 인사 할 사람의 이름 */
  name: string;
  big?: any;
};

const Hello = ({ name, big }: HelloProps) => {
  if (big) {
    return <h1>안녕하세요, {name}!</h1>;
  }
  return <p>안녕하세요, {name}!</p>;
};

Hello.defaultProps = {
  big: false
};

export default Hello;
